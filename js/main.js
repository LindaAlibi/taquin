/**
 * Represents a book.
 * @author Linda ALibi #2681075
 * @filename main.js
 * @description Le taquin est un jeu solitaire en forme de damier. Il est composé de 15 petits carreaux numérotés de 1 à 15 qui glissent dans un cadre prévu pour 16. Il consiste à remettre dans l'ordre les 15 carreaux à partir d'une configuration initiale quelconque avec le moins de mouvement possibles .Un buttons « mélanger » mélange les carreaux jusqu’à l’utilisateur click « arrêter ». Avec la souris, on click sur les carreaux pour déplacer les numéros dans le bon ordre.
 *Le jeu affiche combien de clics a été effectué pour mettre les numéros en ordre.
 */

//Variable declaration

var game,Piece,numberOfClics, text,numbers,button_melange_stop,timer,liVide,colVide;
var numberOfClics=0;

//=======================intiate the game========================== 
function initJeu() {

    var li, col, table, table_body, tr,elementNum;

    //Init le tableau
    table = document.createElement("table");
    table.setAttribute("border", "0");
	table.setAttribute("align", "center");
	table.setAttribute("cellspacing", "0");
    game.appendChild(table);
    
	table_body = document.createElement("tbody");
    table.appendChild(table_body);

	Piece = [[null, null, null, null], [null, null, null, null], [null, null, null, null], [null, null, null, null]];
	numbers = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]];
    
    liVide=3;
    colVide=3;

	for (li = 0; li < 4; li += 1) {
		tr = document.createElement("tr");
        table_body.appendChild(tr);
        //fill the table with td and every td with a button
		for (col = 0; col < 4; col += 1) {
            elementNum = document.createElement("td");
            bouton = document.createElement("button");

            //Call the funtion OnpieceClick when the button (td) is clicked
            bouton.setAttribute("onclick", 'OnPieceClick(' + li + ',' + col + ')');
            elementNum.appendChild(bouton);
            tr.appendChild(elementNum);
            Piece[li][col] = bouton; 
		}
    }
  
    //fill the pieces with numbers 
	fill_Pieces_With_Numbers();
}
//=======================End of initJeu========================== 

//=======================Function to verify the solution========================== 
function VerifieSolution() {
	var li, col, goodPlace;
    goodPlace = 0;
    //verifie if every number of the table is in the right case if yes goodPlace++
	for (li = 0; li < 4; li += 1) {
		for (col = 0; col < 4; col += 1) {
			if ((4 * li + col + 1)===numbers[li][col]) {
				goodPlace += 1;
			}
		}
    }
    //if all the numbers are in right place (===15)then tell the player that he won else show the number of clics and how many pieces are on the right place
	if (goodPlace === 15) {
		text.innerHTML="You did it ! Bravo ! Aprés"+numberOfClics+"Clics";
	} else {
		text.innerHTML=numberOfClics+"Clics , "+goodPlace+"Bien placés";
	}
}
//=======================End of verifieSolution========================== 

//=======================when the player clic on a piece the number of clics++ and the piece move while checking if he won or not  ==========================
function OnPieceClick(li, col) {
numberOfClics+=1;
	MovePieces(li, col);
    UpdatePieces();
    VerifieSolution();
}
//=======================End of OnPieceClick========================== 

//=======================Fill the piece with numbers ========================== 
function fill_Pieces_With_Numbers() {
	var li, col;
	for (li = 0; li < 4; li += 1) {
		for (col = 0; col < 4; col += 1) {
            //fill every piece with a number 
            Piece[li][col].innerHTML = String(numbers[li][col]);
            //set The style of every piece
            if (numbers[li][col] === 0)
            Piece[li][col].className="piece empty";
           else Piece[li][col].className="piece noEmpty";
		}
	}
}
//=======================End of fill_Piece_With_Numbers ========================== 

//=======================Move Piece========================== 
function MovePieces(li, col) {	
	var i, n, dk;
    //if we are on the same colomn as the epty col
    //we change the li
    if (col === colVide) {
		dk = li - liVide;
        if (dk <0 )
        n=-1;
        else n=1;
		for (i = 0; i < n * dk; i += 1) {
			numbers[liVide + n * i][col] = numbers[liVide +  n* (i + 1)][col];
        }
        //we set the liVide to the new empty li
        numbers[liVide + dk][col] = 0;
		liVide = li;
    } 
//if we are on the same li as the empty li 
//we change the col
    else if (li === liVide) {
        dk = col - colVide;
        if (dk <0 )
        n=-1;
        else n=1;
        for (i = 0; i < n * dk; i += 1) 
        {
			numbers[li][colVide + n * i] = numbers[li][colVide + n * (i + 1)];
        }
        //we set the colVide to the new empty col
		numbers[li][colVide + dk] = 0;
		colVide = col;
    }
}
//=======================End of MovePiece========================== 

//=======================UpdatePiece ========================== 
function UpdatePieces() {
	var li, col;
	for (li = 0; li < 4; li += 1) {
		for (col = 0; col < 4; col += 1) {
            //Move Piece
			Piece[li][col].innerHTML = String(numbers[li][col]);
            if (numbers[li][col] === 0)
            //update style 
            Piece[li][col].className="piece empty";
           else Piece[li][col].className="piece noEmpty";
		}
	}
}
//=======================End of UpdatePiece ========================== 

//=======================Mix the numbers randomly ========================== 
function melanger() {
	MovePieces(Math.floor(Math.random() * 4), Math.floor(Math.random() * 4));
    UpdatePieces();
}
//=======================end of melanger ========================== 

//=======================When the player click Melanger button a timer will be set========================== 
function clicMelanger() {
    //show the stop button 
	button_melange_stop.innerHTML = '<button onclick="clicStopMelange()" class="melange">Arrêter de mélanger</button>';
    timer = setInterval(melanger);
}
//=======================End of clicMelanger========================== 

//=======================When the player click StopMelanger button the timer stops at the mixing stops too ========================== 
function clicStopMelange() {
    numberOfClics=0;
	button_melange_stop.innerHTML = '<button onclick="clicMelanger()" class="melange">Mélanger</button>';
    clearInterval(timer);
}
//=======================End of clicStopMelanger========================== 

//=======================Initiale the game and getting all the id ========================== 
function init() {
    button_melange_stop = document.getElementById("controle");
    game = document.getElementById("jeu");
    text=document.getElementById("text");
	initJeu();
}